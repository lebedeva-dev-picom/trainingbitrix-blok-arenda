﻿<html>
<head>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script type="text/javascript" src="fancyBox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancyBox/source/jquery.fancybox.css" media="screen" />

</head>
<body>

	<?php include('top.php') ?>

	<div id="popupform">
		<form method="post" id="myform">
			<p><label>Имя</label>
			<input id="name" value="Вася Пупкин" class="input_text" /></p>
			<p><label>Телефон</label>
			<input id="phone" value="123456" class="input_text" /></p>
			<p><label>E-mail</label>
			<input id="email" value="v.pupkin@yandex.ru" class="input_text" /></p>
			<p><label>Сообщение</label>
			<textarea id="message"  class="input_text"></textarea></p>
			<input id="sendButton" type="button" value="Отправить" />
		</form>
		<div id="answer"></div>
	</div>
	
	<a href="#popupform" id="popupbutton">Заказать звонок</a>

	<?php include('bottom.php') ?>


</body>

<script>

$(document).ready(function() {
	$('#popupbutton').fancybox({
        'padding': 37,
        'overlayOpacity': 0.87,
        'overlayColor': '#fff',
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'inside',
        'centerOnScroll': true,
        'maxWidth': 400,
        'minHeight': 310
    });
	
	$('#name').focus(f);
	$('#email').focus(f);
	$('#phone').focus(f);
	
	function f()
	{
		$('#email').html('');
		
		$('#name').removeClass();
		$('#phone').removeClass();
		$('#email').removeClass();

		$('#name').addClass('input_text');
		$('#phone').addClass('input_text');
		$('#email').addClass('input_text');
	}
	
	$('#sendButton').click(function() {
		
		var params = 
			'name=' + encodeURIComponent($('#name').val()) + 
			'&phone=' + encodeURIComponent($('#phone').val()) +
			'&email=' + encodeURIComponent($('#email').val());
			
		var req = new XMLHttpRequest();
		req.onreadystatechange = function()
		{
			if (req.readyState == 4 && req.status == 200)
			{
				var errcode = req.responseText;
				
				switch (errcode)
				{
					case '0':
						$('#answer').html('Спасибо. Администратор свяжется с Вами в ближайшее время');
						$('#myform').css('display', 'none');
						break;
					case '1':
						$('#name').removeClass('input_text');
						$('#name').addClass('error_input_text');
						$('#answer').html('Поле "Имя" должно быть заполнено');
						break;
					case '2':
						$('#phone').removeClass('input_text');
						$('#phone').addClass('error_input_text');
						$('#answer').html('Поле "Телефон" должно быть заполнено');
						break;
					case '3':
						$('#email').removeClass('input_text');
						$('#email').addClass('error_input_text');
						$('#answer').html('Поле "E-mail" должно быть заполнено');
						break;
					case '4':
						$('#phone').removeClass('input_text');
						$('#phone').addClass('error_input_text');
						$('#answer').html('Неверный телефон');
						break;
					case '5':
						$('#email').removeClass('input_text');
						$('#email').addClass('error_input_text');
						$('#answer').html('Неверный E-mail');
						break;
				} //switch
			} //if
		} //req.onreadystatechange
		
		req.open('POST', 'scr/script-18.php', true);
		req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		req.send(params);
		
    });
});
</script>











