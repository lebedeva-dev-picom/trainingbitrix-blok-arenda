﻿<?php

$name = $_POST['name'];
$phone = $_POST['phone'];
$email = $_POST['email'];

if ($name == '') echo 1;
elseif ($phone == '') echo 2;
elseif ($email == '') echo 3;
elseif (!chkPhone($phone)) echo 4;
elseif (!chkEmail($email)) echo 5;
else echo 0;

function chkPhone($p)
{
	$reg = '/^[0-9][0-9\s-\(\)]*[0-9]$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return true;
	else return false;
}

function chkEmail($p)
{
	$reg = '/^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return true;
	else return false;
}

?>