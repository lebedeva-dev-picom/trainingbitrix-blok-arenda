﻿<?php

$Surname = htmlspecialchars(strip_tags($_POST['p1']));
$Name = htmlspecialchars(strip_tags($_POST['p2']));
$MidName = htmlspecialchars(strip_tags($_POST['p3']));
$Phone = htmlspecialchars(strip_tags($_POST['p4']));
$Email = htmlspecialchars(strip_tags($_POST['p5']));
$BirthDate = htmlspecialchars(strip_tags($_POST['p6']));

$err = '';

if (fEmpty($Surname) || fEmpty($Name) || fEmpty($MidName) || fEmpty($Phone) || fEmpty($Email) || fEmpty($BirthDate))
	$err = '<p>Не все поля заполнены!</p>';
		
elseif (fNoRus($Surname))
	$err = '<p>Фамилия должна быть на русском языке!</p>';
		
elseif (fNoRus($Name))
	$err = '<p>Имя должно быть на русском языке!</p>';
		
elseif (fNoRus($MidName))
	$err = '<p>Отчество должно быть на русском языке!</p>';
	
elseif (noCorrectEml($Email))
	$err = '<p>Неверный e-mail!</p>';
	
elseif (noCorrectPhone($Phone))
	$err = '<p>Неверный номер телефона!</p>';

if ($err == '')
	echo printWelcome($Surname, $Name, $MidName, $BirthDate, $Phone, $Email);
else
{
	$form = printForm($Surname, $Name, $MidName, $Phone, $Email, $BirthDate);
	echo $form . $err;
}

echo '<p><a href="../task16.php">Назад</a></p>';

///////////////////////////////////////////////////

function fEmpty($p)
{
	if ($p == '')
		return true;
	else return false;
}

function fNoRus($p)
{
	$reg = '/^[А-Яа-яЁё][А-Яа-яЁё-\s]*/u';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}

function noCorrectPhone($p)
{
	$reg = '/^[0-9][0-9\s-\(\)]*[0-9]$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}

function noCorrectEml($p)
{
	$reg = '/^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}
function getAge($p)
{
	$now_date = time();
	$brt_date = strtotime($p);
	$age = floor(($now_date - $brt_date) / (60 * 60 * 24 * 365.25));
	return $age;
}

function getUsername($p1, $p2, $p3, $p6)
{
	if (getAge($p6) > 18)
	return $p1.' '.$p2.' '.$p3;
	else return $p2;
}

function printForm($p1, $p2, $p3, $p4, $p5, $p6)
{
	$s = '<div>';
	$s .= '<h3>Приветствие</h3>';
	$s .= '<form action="" method="post">';
	$s .= '<p>Фамилия:</br><input name="p1" value="'.$p1.'"></p>';
	$s .= '<p>Имя:</br><input name="p2" value="'.$p2.'"></p>';
	$s .= '<p>Отчество:</br><input name="p3" value="'.$p3.'"></p>';
	$s .= '<p>E-mail:</br><input name="p5" value="'.$p5.'"></p>';
	$s .= '<p>Телефон:</br><input name="p4" value="'.$p4.'"></p>';
	$s .= '<p>Дата рождения:</br><input type="date" name="p6" value="'.$p6.'"></p>';
	$s .= '<p><input type="submit" /></p>';
	$s .= '</form>';
	$s .= '</div>';
	return $s;
}

function printWelcome($p1, $p2, $p3, $p6, $p4, $p5)
{
	$s = '<div id="msg"><p>Здравстуйте, '.getUsername($p1, $p2, $p3, $p6).'!</p></div>';
	$s .= '<p>Дата рождения: '.$p6.' ('.getAge($p6).')</p>';
	$s .= '<p>Телефон: '.$p4.'</p>';
	$s .= '<p>E-mail: '.$p5.'</p>';
	return $s;
}

?>