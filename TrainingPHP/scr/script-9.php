﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$t = htmlspecialchars(strip_tags($_POST['url']));
$p = '~(?:(?:ftp|https?)?://|www\.)[a-z_.]+?[a-z_]{2,6}(:?/[a-z0-9\-?\[\]=&;#]+)?~i';

echo '<p>URL: <a href="'.$t.'">'.$t.'</a></p><p>Результат распознавания: ';

if (preg_match_all($p, $t))
	echo 'TRUE';
else
	echo 'FALSE';

?>