﻿<?php
// print_r($_POST);

$Surname = $_POST['p1'];
$Name = $_POST['p2'];
$MidName = $_POST['p3'];
$Phone = $_POST['p4'];
$Email = $_POST['p5'];
$BirthDate = $_POST['p6'];

$errcode = 0;

if (fEmpty($Surname) || fEmpty($Name) || fEmpty($MidName) || fEmpty($Phone) || fEmpty($Email) || fEmpty($BirthDate))
	{ echo '<p>Поля не заполнены!</p>'; $errcode = 1; }
		
if (fNoRus($Surname))
	{ echo '<p>Фамилия должна быть на русском языке!</p>'; $errcode = 1; }
	
if (fNoRus($Name))
	{ echo '<p>Имя должно быть на русском языке!</p>'; $errcode = 1; }
	
if (fNoRus($MidName))
	{ echo '<p>Отчество должно быть на русском языке!</p>'; $errcode = 1; }

if (noCorrectEml($Email))
	{ echo '<p>Неверный e-mail!</p>'; $errcode = 1; }

if (noCorrectPhone($Phone))
	{ echo '<p>Неверный номер телефона!</p>'; $errcode = 1; }

if (!($errcode > 0))
	echo printWelcome($Surname, $Name, $MidName, $BirthDate, $Phone, $Email);

///////////////////////////////////////////////////

function fEmpty($p)
{
	if ($p == '')
		return true;
	else return false;
}

function fNoRus($p)
{
	$reg = '/^[А-Яа-яЁё][А-Яа-яЁё-\s]*/u';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}

function noCorrectPhone($p)
{
	$reg = '/^[0-9][0-9\s-\(\)]*[0-9]$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}

function noCorrectEml($p)
{
	$reg = '/^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/';
	preg_match($reg, trim($p), $out);
	if ($out[0] == trim($p))
		return false;
	else return true;
}
function getAge($p)
{
	$now_date = time();
	$brt_date = strtotime($p);
	$age = floor(($now_date - $brt_date) / (60 * 60 * 24 * 365.25));
	return $age;
}

function getUsername($p1, $p2, $p3, $p6)
{
	if (getAge($p6) > 18)
	return $p1.' '.$p2.' '.$p3;
	else return $p2;
}

function printWelcome($p1, $p2, $p3, $p6, $p4, $p5)
{
	$s = '<p>Здравстуйте, '.getUsername($p1, $p2, $p3, $p6).'!</p>';
	$s .= '<p>Дата рождения: '.$p6.' ('.getAge($p6).')</p>';
	$s .= '<p>Телефон: '.$p4.'</p>';
	$s .= '<p>E-mail: '.$p5.'</p>';
	return $s;
}

?>