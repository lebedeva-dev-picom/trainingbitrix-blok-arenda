﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$url = htmlspecialchars(strip_tags($_POST['url']));

echo '<p>Адрес: <a href="'.$url.'">'.$url.'</a></p>';

preg_match('/(?<=.)\d/', $url, $a);
preg_match('/\d(?=\_)/', $url, $b);
preg_match_all('/(?<=n)\d/', $url, $c);

echo '<p>Первая цифра: '.( count($a) > 0 ? $a[0] : 'не найдено').'</p>';
echo '<p>Цифра до подчеркивания: '.( count($b) > 0 ? $b[0] : 'не найдено').'</p>';
echo '<p>Цифра после последнего "n": '.( count($c[0]) > 0 ? $c[0][count($c[0]) - 1] : 'не найдено').'</p>';

?>