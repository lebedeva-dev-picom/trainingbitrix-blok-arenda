﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$x = htmlspecialchars(strip_tags($_POST['x']));

echo 'Ответ: ';
echo f($x);

function f($x)
{
	$d = '(я молодец)';
	$x = preg_replace("/  +/", " ", $x);
	$words = explode(' ', $x);
	if(count($words) > 0)
	{
		$retval = $words[0];
		for($i = 1; $i < count($words); $i++)
			$retval .= ' '.$d.' '.$words[$i];
		return $retval;
	}
	else return $d;
}

?>