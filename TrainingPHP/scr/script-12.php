﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$tel = htmlspecialchars(strip_tags($_POST['tel']));
// echo '<p>Номер телефона: "'.$tel.'"</p>';

// $reg = '/(((один|I|1)|(два|II|2)|(три|III|3)|(четыре|IV|4)|(пять|V|5)|(шесть|VI|6)|(семь|VII|7)|(восемь|VIII|8)|(девять|IX|9)|(ноль|O|0))(\s|-|\(|\))*)+/';

// $res = preg_match_all($reg, $tel);

// echo '<p>Результат: '.($res > 0 ? 'TRUE' : 'FALSE').'</p>';

$separator = '[ \-\(\)]*';
$num_word = 'один|два|три|четыре|пять|шесть|семь|восемь|девять|ноль';
$rome = '(?<![IXV])(?:I?[XV]|[XV]?I{1,3})(?![IXV])';
$reg = '/(?:(?:\d|' . $num_word . '|' . $rome . ')' . $separator . ')+/';

// $str = 'два-три-I 2-3-1рит';

// preg_match($reg, $tel, $arr);
// print_r(($arr[0] == $tel?'true':'false'));
// echo '</p>';
preg_match($reg, $tel, $arr);
echo '<p>Результат: '.($arr[0] == $tel ? 'TRUE' : 'FALSE').'</p>';
// print_r(($arr[0] == $tel?'true':'false'));
// //----------------------------------------------------------------
// //+7 (555) 555-555
// //555
// echo '<pre>';
// $a = '+7 (555) 555-555';
// $b = '/\+7\s\(\d{3}\)\s\d{3}\-\d{3}/';
// preg_match($b, $a, $r);
// print_r($r);
// echo ($r[0] == $a ? 'TRUE' : 'FALSE');
// echo '</pre>';
?>