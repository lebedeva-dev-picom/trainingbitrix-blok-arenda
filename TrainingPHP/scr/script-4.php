﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$arr = genArr();

$a1 = getTbl($arr);
$a2 = getDiag1($arr);
$a3 = getDiag2($arr);

//echo "({'answer1':'".$a1."','answer2':'".$a2."','answer3':'".$a3."'})";
echo $a1;
echo  '<p><span class="color1">' . $a2 . '</span></p>';
echo  '<p><span class="color2">' . $a3 . '</span></p>';

////////////////////////////////////////////////

function getRandomChar()
{
	return chr(mt_rand(97,122));
}

function genArr()
{
	for ($i = 0; $i < 10; $i++)
	{
		for ($j = 0; $j < 10; $j++)
			$arr[$i][$j] = getRandomChar();
	}
	return $arr;
}

function getTbl($arr)
{
	$retval = '<table border="0px">';
	for ($i = 0; $i < 10; $i++)
	{
		$retval .= '<tr>';
		for ($j = 0; $j < 10; $j++)
		{
			$retval .= '<td';
			if ($i == $j)
				$retval .= ' class="color1"';
			else if (($i + $j) == 9)
				$retval .= ' class="color2"';
			$retval .= '>';
			$retval .= $arr[$i][$j];
			$retval .= '</td>';
		}
		$retval .= '</tr>';
	}
	$retval .= '</table>';
	return $retval;
}

function getDiag1($arr)
{
	$retval = '';
	for ($i = 0; $i < 10; $i++)
		for ($j = 0; $j < 10; $j++)
			if ($i == $j)
				$retval .= $arr[$i][$j].' ';
	return $retval;
}

function getDiag2($arr)
{
	$retval = '';
	for ($i = 0; $i < 10; $i++)
		for ($j = 0; $j < 10; $j++)
			if (($i + $j) == 9)
				$retval .= $arr[$i][$j].' ';
	return $retval;
}

?>