﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$text = htmlspecialchars(strip_tags($_POST['t']));
$pattern = htmlspecialchars(strip_tags($_POST['p']));

chk('/'.$pattern.'/', $text);

function chk($p, $t)
{
	$count = preg_match_all($p, $t, $arr);
	if ($count > 0)
	{
		echo '<p>Совпадений: '.$count.'</p>';
		foreach($arr[0] as $a)
			echo '<li>'.$a.'</li>';
	}
	else echo '<p>Совпадений не найдено</p>';
}

?>