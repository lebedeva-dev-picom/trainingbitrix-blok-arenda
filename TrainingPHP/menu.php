﻿<div id="menu">
	<ol>
	<h4>Начальные знания</h4>
		<li><a href="task1.php">Bычислить значение функций</a></li>
		<li><a href="task2.php">Инвертировать строку</a></li>
		<li><a href="task3.php">Вставить строку между словами</a></li>
		<li><a href="task4.php">Диагонали таблицы</a></li>
		<li><a href="task5.php">Текущая дата</a></li>
		<li><a href="task6.php">Факториал числа</a></li>
		<li><a href="task7.php">Текстовый файл</a></li>
	<h4>Регулярные выражения</h4>
		<li><a href="task8.php">Проверка текста</a></li>
		<li><a href="task9.php">Распознавание URL</a></li>
		<li><a href="task10.php">Разбор адреса</a></li>
		<li><a href="task11.php">URL (2)</a></li>
		<li><a href="task12.php">Проверка номера телефона</a></li>
	<h4>Работа с формами</h4>
		<li><a href="task13.php">JS</a></li>
		<li><a href="task14.php">JS + регулярные выражения</a></li>
		<li><a href="task15.php">JS + jQuery</a></li>
		<li><a href="task16.php">PHP</a></li>
		<li><a href="task17.php">PHP + AJAX</a></li>
	<h4>Работа с формами + AJAX</h4>
		<li><a href="task18.php">форма "Заказать звонок"</a></li>
	<!--
	<h4>основы работы с MySQL</h4>
		<li><a href="task19.php">форма "Заказать звонок"</a></li>
		<li><a href="task20.php">Пользователь</a></li>
		<li><a href="task21.php">Администратор</a></li>
	-->
	</ol>
</div>
</div>