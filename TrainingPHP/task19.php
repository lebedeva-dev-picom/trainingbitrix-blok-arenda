﻿<html>
<head>

	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
	<script type="text/javascript" src="fancyBox/source/jquery.fancybox.pack.js"></script>
	<script type="text/javascript" src="fancyBox/source/jquery.fancybox.js"></script>
	<link rel="stylesheet" type="text/css" href="fancyBox/source/jquery.fancybox.css" media="screen" />

</head>
<body>

	<?php include('top.php') ?>

	<div id="popupform">
		<form method="post" id="myform" enctype="multipart/form-data">
			<p><label>Имя</label>
				<input id="name" value="Вася Пупкин" class="input_text" /></p>
			<p><label>Загрузить фотографию<label>
				<img id="image" style="display:none" />
				<input id="userfile" name="userfile" type="file" accept="image/*" class="input_text" /></p>
			<p><label>Телефон</label>
				<input id="phone" value="123456" class="input_text" /></p>
			<p><label>E-mail</label>
				<input id="email" value="v.pupkin@mail.ru" class="input_text" /></p>
			<p><label>Сообщение</label>
				<textarea id="message"  class="input_text">Привет!</textarea></p>
			<input id="sendButton" type="button" value="Отправить" />
		</form>
		<div id="answer"></div>
	</div>
	
	<a href="#popupform" id="popupbutton">Заказать звонок</a>
	
	<div id="result"></div>

	<?php include('bottom.php') ?>


</body>

<script>

var img_file;
	
function f()
{
	$('#email').html('');
	
	$('#name').removeClass();
	$('#phone').removeClass();
	$('#email').removeClass();

	$('#name').addClass('input_text');
	$('#phone').addClass('input_text');
	$('#email').addClass('input_text');
}

function sendButtonClick()
{
	var params = 
		'name=' + encodeURIComponent($('#name').val()) + 
		'&phone=' + encodeURIComponent($('#phone').val()) +
		'&email=' + encodeURIComponent($('#email').val()) +
		'&message=' + encodeURIComponent($('#message').val()) +
		'&userfile=' + encodeURIComponent($('#userfile').val());
		
	var req = new XMLHttpRequest();
	req.onreadystatechange = function()
	{
		if (req.readyState == 4 && req.status == 200)
		{
			var obj = eval(req.responseText);
			switch (obj.errcode)
			{
				case '0':
					//$('#answer').html('Спасибо. Администратор свяжется с Вами в ближайшее время');
					$('#answer').html('[' + obj.result + ']');
					$('#myform').css('display', 'none');
					break;
				case '1':
					$('#name').removeClass('input_text');
					$('#name').addClass('error_input_text');
					$('#answer').html('Поле "Имя" должно быть заполнено');
					break;
				case '2':
					$('#phone').removeClass('input_text');
					$('#phone').addClass('error_input_text');
					$('#answer').html('Поле "Телефон" должно быть заполнено');
					break;
				case '3':
					$('#email').removeClass('input_text');
					$('#email').addClass('error_input_text');
					$('#answer').html('Поле "E-mail" должно быть заполнено');
					break;
				case '4':
					$('#phone').removeClass('input_text');
					$('#phone').addClass('error_input_text');
					$('#answer').html('Неверный телефон');
					break;
				case '5':
					$('#email').removeClass('input_text');
					$('#email').addClass('error_input_text');
					$('#answer').html('Неверный E-mail');
					break;
			} //switch
		} //if
	} //req.onreadystatechange

	req.open('POST', '/scr/script-19.php', true);
	// req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	req.setRequestHeader('Content-Type', 'multipart/form-data');
	req.send(params);
}

function uploadPreview(input)
{
	if (input.files && input.files[0])
	{
		var reader = new FileReader();
		reader.onload = function (e)
		{
			$('#image').attr('src', e.target.result);
			$('#image').css({'display' : 'block', 'width' : '100px', 'height' : '100px'});
		}
		reader.readAsDataURL(input.files[0]);
    }
		
	img_file = this.files;
}
	
$(document).ready(function() {
	$('#popupbutton').fancybox({
        'padding': 37,
        'overlayOpacity': 0.87,
        'overlayColor': '#fff',
        'transitionIn': 'none',
        'transitionOut': 'none',
        'titlePosition': 'inside',
        'centerOnScroll': true,
        'maxWidth': 400,
        'minHeight': 310
    });
	
	$('#name').focus(f);
	$('#email').focus(f);
	$('#phone').focus(f);
	
	$('#sendButton').click(sendButtonClick);
	
	$("#userfile").change(function() { uploadPreview(this); });
});
</script>











