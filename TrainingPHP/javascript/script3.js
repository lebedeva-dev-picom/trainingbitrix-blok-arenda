﻿document.getElementById("pf").onclick = function()
{
	document.getElementById("task").innerHTML = 
	'<div>' +
	'<h3>Приветствие (AJAX)</h3>' +
	'<form>' +
	'<p>Фамилия:</br><input name="p1" value="Иванов"></p>' +
	'<p>Имя:</br><input name="p2" value="Иван"></p>' +
	'<p>Отчество:</br><input name="p3" value="Иванович"></p>' +
	'<p>E-mail:</br><input name="p5" value="qwerty@mail.mail.ru"></p>' +
	'<p>Телефон:</br><input name="p4" value="8 (999) 123-45-67"></p>' +
	'<p>Дата рождения:</br><input type="date" name="p6" value="1990-07-07"></p>' +
	'<p><input type="button" onclick="ff(p1.value, p2.value, p3.value, p4.value, p5.value, p6.value)" value="Отправить" /></p>' +
	'</form>' +
	'<div id="msg"></div>' +
	'</div>';
}

function ff(p1, p2, p3, p4, p5, p6)
{
	var params = 
		'p1=' + encodeURIComponent(p1) + 
		'&p2=' + encodeURIComponent(p2) + 
		'&p3=' + encodeURIComponent(p3) + 
		'&p4=' + encodeURIComponent(p4) + 
		'&p5=' + encodeURIComponent(p5) + 
		'&p6=' + encodeURIComponent(p6);
	var msg = document.getElementById('msg');
	var req = new XMLHttpRequest();
	
	req.onreadystatechange = function()
	{
		if (req.readyState == 4 && req.status == 200)
		{
			msg.innerHTML = req.responseText;
		}
	}
	req.open("POST", '/scr/script-f.php', true);
	req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	req.send(params);
	
}
/////////////////////////////////////

document.getElementById("pe").onclick = function()
{
	document.getElementById("task").innerHTML = 
	'<div>' +
	'<h3>Приветствие (JS)</h3>' +
	'<form>' +
	// '<form action="scr/script-d.php" method="post">' +
	'<p>Фамилия:</br><input name="p1" value="Иванов"></p>' +
	'<p>Имя:</br><input name="p2" value="Иван"></p>' +
	'<p>Отчество:</br><input name="p3" value="Иванович"></p>' +
	'<p>E-mail:</br><input name="p5" value="qwerty@mail.mail.ru"></p>' +
	'<p>Телефон:</br><input name="p4" value="8 (999) 123-45-67"></p>' +
	'<p>Дата рождения:</br><input type="date" name="p6" value="1990-07-07"></p>' +
	'<p><input type="button" onclick="fd(p1.value, p2.value, p3.value, p4.value, p5.value, p6.value)" value="Отправить" /></p>' +
	// '<p><input type="submit" /></p>' +
	'</form>' +
	'<div id="msg"></div>' +
	'</div>';
}

document.getElementById("pd").onclick = function()
{
	document.getElementById("task").innerHTML = 
	'<div>' +
	'<h3>Приветствие (PHP)</h3>' +
	//'<form>' +
	'<form action="scr/script-d.php" method="post">' +
	'<p>Фамилия:</br><input name="p1" value="Иванов"></p>' +
	'<p>Имя:</br><input name="p2" value="Иван"></p>' +
	'<p>Отчество:</br><input name="p3" value="Иванович"></p>' +
	'<p>E-mail:</br><input name="p5" value="qwerty@mail.mail.ru"></p>' +
	'<p>Телефон:</br><input name="p4" value="8 (999) 123-45-67"></p>' +
	'<p>Дата рождения:</br><input type="date" name="p6" value="1990-07-07"></p>' +
	//'<p><input type="button" onclick="fd(p1.value, p2.value, p3.value, p4.value, p5.value, p6.value)" value="Отправить" /></p>' +
	'<p><input type="submit" /></p>' +
	'</form>' +
	'</div>';
}

function chkRus(p)
{
	var reg = /[\u0400-\u04FF]+/;
	return p.match(reg) == p;
}

function chkTel(p)
{
	var reg = /^[0-9][0-9\s-\(\)]*[0-9]$/;
	return p.match(reg) == p;
}

function chkEml(p)
{
	var reg = /^[a-z]+[a-z0-9_-]*[\.[a-z0-9_-]+]*@[a-z0-9_-]+[\.[a-z0-9_-]+]*\.[a-z]{2,6}$/;
	return p.match(reg) == p;
}

function getAge(p)
{
	var now_date = new Date;
	var brt_date = new Date(p);
	var age = now_date - brt_date;
	return Math.floor(age / 1000 / 24 / 60 / 60 / 365);
}

function getUsername(p1, p2, p3, p6)
{
	if (getAge(p6) > 18)
		return p1 + ' ' + p2 + ' ' + p3;
	else return p2;
}

function fd(p1, p2, p3, p4, p5, p6)
{
	var str;
	if (p1 == '' || p2 == '' || p3 == '' || p4 == '')
		str = 'Поля не заполнены!';
		
	else if (!(chkRus(p1) && chkRus(p2) && chkRus(p3)))
		str = 'ФИО должны быть на русском языке!';
		
	else if (!chkTel(p4))
		str = 'Неверный номер телефона!';
		
	else if (!chkEml(p5))
		str = 'Неверный e-mail!';
		
	else
		str = '<p>Здравстуйте, ' + getUsername(p1, p2, p3, p6) + '!' +
		//'</p><p>Ваш e-mail: ' + p5 + '</p>' +
		//'</p><p>Ваш телефон: ' + p4 + '</p>' +
		//'</p><p>Ваш возраст: ' + getAge(p6) + ' лет</p>' +
		'</p>';
	
	document.getElementById("msg").innerHTML = str;
}