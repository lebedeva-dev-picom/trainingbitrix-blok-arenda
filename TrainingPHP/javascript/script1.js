﻿		document.getElementById("p1").onclick = p1click;
		document.getElementById("p2").onclick = p2click;
		document.getElementById("p3").onclick = p3click;
		document.getElementById("p4").onclick = p4click;
		document.getElementById("p5").onclick = p5click;
		document.getElementById("p6").onclick = p6click;
		document.getElementById("p7").onclick = p7click;


		function sol11(x, n)
		{
			var y = 0;
			for (var i = 1; i <= n; i++)
			{
				y += (Math.exp(i * x) + Math.tan(i * x)) / (3 * i * x);
			}
			return y;
		}

		function sol12(x, n)
		{
			var y = 0;
			for (var i = 1; i <= n; i++)
			{ 
				y += ((Math.pow(i + 2, 2)) / i) * x;
			}
			return y;
		}

		function sol13(x)
		{
			var f = 0;
			if (x < 2)
			{
				return 5 * x + 3;
			}
			else if (x > 5)
			{
				return Math.sin((2 * x) / 3);
			}
			else
			{
				f = 0;
				for (var j = 1; j < 10; j++)
				{
					f += x / (x + Math.pow(j, 2));
				}
				return f;
			}
		}

		function sol21(x)
		{
			return x.split('').reverse().join('');
		}

		function sol31(x)
		{
			var d = '(я молодец)';
			var words = x.split(' ');
			if(words.length > 0)
			{
				var retval = words[0];
				for(var i = 1; i < words.length; i++)
					retval += ' ' + d + ' ' + words[i];
				return retval;
			}
			else return d;

		}

		//start sol41

		function getRandomChar()
		{
			var min = 97;
			var max = 122;
			var code = Math.floor(Math.random() * (max - min + 1)) + min;
			var c = String.fromCharCode(code);
			return c;
		}

		function genArr()
		{
			var arr = [];
			for (var i = 0; i < 10; i++)
			{
				arr[i] = [];
				for (var j = 0; j < 10; j++)
					arr[i][j] = getRandomChar();
			}
			return arr;
		}

		function getTbl(arr)
		{
			var retval = '<table border="0px">';
			for (var i = 0; i < 10; i++)
			{
				retval += '<tr>';
				for (var j = 0; j < 10; j++)
				{
					retval += '<td';
					if (i == j)
						retval += ' class="color1"';
					else if ((i + j) == 9)
						retval += ' class="color2"';
					retval += '>';
					retval += arr[i][j];
					retval += '</td>';
				}
				retval += '</tr>';
			}
			retval += '</table>';
			return retval;
		}

		function getDiag1(arr)
		{
			var retval = '';
			for (var i = 0; i < 10; i++)
				for (var j = 0; j < 10; j++)
					if (i == j)
						retval += arr[i][j] + ' ';
			return retval;
		}

		function getDiag2(arr)
		{
			var retval = '';
			for (var i = 0; i < 10; i++)
				for (var j = 0; j < 10; j++)
					if ((i + j) == 9)
						retval += arr[i][j] + ' ';
			return retval;
		}

		//end sol41

		function sol61(x)
		{
			var retval = 1;
			for(var i = 1; i <= x; i++)
				retval *= i;
			return retval;
		}

		function p1click()
		{
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Bычислить значение функций</h3>' +
			'<p>x = <input id="x" type="number" autocomplete="off" value="0" /></p>' +
			'<p>n = <input id="n" type="number" autocomplete="off" value="0" /></p>' +
			'<input id="s1" type="button" value="Отправить" />' +
			'<p><img src="img/formula-1.gif"></p>' +
			'<p>y(x, n) = <span id="answ1"></span></p>' +
			'<p><img src="img/formula-2.gif"></p>' +
			'<p>y(x, n) = <span id="answ2"></span></p>' +
			'<p><img src="img/formula-3.gif"></p>' +
			'<p>f(x) = <span id="answ3"></span></p>' +
			'</div>';
			document.getElementById("s1").onclick = function() {
				var x = document.getElementById("x").value;
				var n = document.getElementById("n").value;
				document.getElementById("answ1").innerHTML = sol11(x, n);
				document.getElementById("answ2").innerHTML = sol12(x, n);
				document.getElementById("answ3").innerHTML = sol13(x);
			}
		}

		function p2click()
		{
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Инвертировать строку</h3>' +
			'<p>Строка: <input id="x" type="text" autocomplete="off" value="строка" /></p>' +
			'<input id="s1" type="button" value="Отправить" />' +
			'<p>Ответ: <span id="answ1"></span></p>' +
			'</div>';
			document.getElementById("s1").onclick = function() {
				var x = document.getElementById("x").value;
				document.getElementById("answ1").innerHTML = sol21(x);
			}
		}

		function p3click()
		{
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Вставить строку между словами</h3>' +
			'<p>Строка: <input id="x" type="text" autocomplete="off" value="я иду домой после работы" /></p>' +
			'<input id="s1" type="button" value="Отправить" />' +
			'<p>Ответ: <span id="answ1"></span></p>' +
			'</div>';
			document.getElementById("s1").onclick = function() {
				var x = document.getElementById("x").value;
				document.getElementById("answ1").innerHTML = sol31(x);
			}
		}

		function p4click()
		{
			var arr = genArr();
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Диагонали таблицы</h3>' +
			'<p>' + getTbl(arr) + '</p>' +
			'<input id="s1" type="button" value="Отправить" />' +
			'<p><span id="answ1" class="color1"></span></p>' +
			'<p><span id="answ2" class="color2"></span></p>' +
			'</div>';
			document.getElementById("s1").onclick = function() {
				document.getElementById("answ1").innerHTML = getDiag1(arr);
				document.getElementById("answ2").innerHTML = getDiag2(arr);

				document.getElementById("answ1").style = "padding: 5";
				document.getElementById("answ2").style = "padding: 5";
			}
		}

		function p5click()
		{
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Текущая дата</h3>' +
			'<p>toLocaleString("ru", options1): <span id="answ1"></span></p>' +
			'<p>toISOString: <span id="answ2"></span></p>' +
			'<p>toString: <span id="answ3"></span></p>' +
			'<p>toDateString: <span id="answ4"></span></p>' +
			'<p>toTimeString: <span id="answ5"></span></p>' +
			'</div>';
			var now = new Date();
			var options1 = { era: 'long', year: 'numeric', month: 'long', day: 'numeric', weekday: 'long', 
				timezone: 'UTC', hour: 'numeric', minute: 'numeric', second: 'numeric' };
			document.getElementById("answ1").innerHTML = now.toLocaleString("ru", options1);
			document.getElementById("answ2").innerHTML = now.toISOString();
			document.getElementById("answ3").innerHTML = now.toString();
			document.getElementById("answ4").innerHTML = now.toDateString();
			document.getElementById("answ5").innerHTML = now.toTimeString();
		}

		function p6click()
		{
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Факториал числа</h3>' +
			'<p>x = <input id="x" type="number" autocomplete="off" value="5" /></p>' +
			'<input id="s1" type="button" value="Отправить" />' +
			'<p>x! = <span id="answ1"></span></p>' +
			'</div>';
			document.getElementById("s1").onclick = function() {
				var x = document.getElementById("x").value;
				document.getElementById("answ1").innerHTML = sol61(x);
			}
		}

		function p7click()
		{
			document.getElementById("task").innerHTML = 
			'<div>' +
			'<h3>Текстовый файл</h3>' +
			'<form enctype="multipart/form-data" action="upload.php" method="post">' +
			'<input type="hidden" name="MAX_FILE_SIZE" value="30000" />' +
			'<p>Файл для проверки: <input type="file" name="userfile" /></p>' +
			'<p>Поиск: <input type="text" name="text1" value="я молодец" /></p>' +
			'<input type="submit" value="Проверить" />' +
			'</form>' +
			'</div>';
		}