﻿<link rel="stylesheet" type="text/css" href="../style.css">
<?php

$uploads = 'TrainingPHP' . DIRECTORY_SEPARATOR . 'uploads';
$uploaddir = $_SERVER['DOCUMENT_ROOT'] . DIRECTORY_SEPARATOR . $uploads . DIRECTORY_SEPARATOR;
$full_path = $uploaddir . basename($_FILES['userfile']['name']);

if (move_uploaded_file($_FILES['userfile']['tmp_name'], $full_path))
{
	echo '<p>Файл "' . $_FILES['userfile']['name'] . '"</p>';
	
	$search = $_POST['text'];

	$handle = fopen($full_path, 'r');
	
	$orig = fgets($handle);
	
	fclose($handle);
	$count = substr_count($orig, $search);
	$result = str_replace($search, '', $orig);
	$result = str_replace('  ', ' ', $result);

	file_put_contents($full_path, $result);

	echo '<p>' . $orig . '</p>';
	echo '<p>' . $result . '</p>';

	echo '<p>Строка "' . $search . '" удалена ' . $count . ' раз(а)</p>';


	$download = $uploads . DIRECTORY_SEPARATOR . basename($_FILES['userfile']['name']);
	echo '<p><a href="' . $download . '" download>Скачать</a></p>';
}
else
{
	echo '<p>Не удалось загрузить файл</p>';
}

?>
