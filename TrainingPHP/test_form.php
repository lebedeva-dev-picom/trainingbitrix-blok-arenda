﻿<?php include("top.php") ?>

<div id="popupform1">
<form id="myform" name="myform">
	<!--input type="text" name="txt" id="txt" placeholder="Имя" /></br>
	<input type="file" name="img_file" id="img" accept="image/*" value="Загрузить изображение" /></br>
	<img id="preview" style="display:none" />
	<input type="button" name="submit" id="submit" value="Отправить"-->
	
	<p><label>Имя</label>
		<input id="name" value="Вася Пупкин" class="input_text" /></p>
	<p><label>Загрузить фотографию<label></br>
		<img id="preview" style="display:none" />
		<input id="userfile" name="userfile" type="file" accept="image/*" class="input_text" /></p>
	<p><label>Телефон</label>
		<input id="phone" value="123456" class="input_text" /></p>
	<p><label>E-mail</label>
		<input id="email" value="v.pupkin@mail.ru" class="input_text" /></p>
	<p><label>Сообщение</label>
		<textarea id="message"  class="input_text">Привет!</textarea></p>
	<input name="submit" id="submit" type="button" value="Отправить" />
</form>
</div>

<div id="txt_result"></div>
<div id="img_result"></div>

<?php include("bottom.php") ?>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script>

var script_url = 'scr/test_form_scr.php';

$('#userfile').change(function() { uploadPreview(this); });
$('#submit').click(submit);

function submit()
{
	var fd = new FormData($('#myform')[0]);
	$.ajax({
		type: 'POST',
		processData: false,
		contentType: false,
		url: script_url,
		data: fd,
		success: success
	});
}

function success(data)
{
	$('#txt_result').html(data);
}

function uploadPreview(input)
{
	if (input.files && input.files[0])
	{
		var reader = new FileReader();
		reader.onload = function (e)
		{
			$('#preview').attr('src', e.target.result);
			$('#preview').css({'display' : 'block', 'width' : '100px', 'height' : '100px'});
		}
		reader.readAsDataURL(input.files[0]);
    }
}

</script>