function isMobile() {
    if ($(window).width() <= 768 || /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        return true;
    }
}

function toggleCallbackForm() {

    var $openFormBtn = $('#js-openCallbackFormBtn'),
        $form = $('#js-callbackForm'),
        $closeFormBtn = $('#js-closeCallbackFormBtn'),
        $navItems = $('#js-mobMenu > a');

    $openFormBtn.on('click', function (e) {
        e.preventDefault();

        $form.slideDown();
        $(window).scrollTop(0);
        $navItems.removeClass('active');

        $closeFormBtn.on('click', function (e) {
            e.preventDefault();

            $form.slideUp();
        });
    });
}

function initMap() {

    var map,
        marker,
        styledMap,
        styles,
        izh = { lat: 56.876923, lng: 53.283218 };

    styles = [{
        stylers: [{ saturation: -100 }]
    }];

    styledMap = new google.maps.StyledMapType(styles, { name: 'Styled Map' });
    map = new google.maps.Map(document.getElementById('map'), {
        center: izh,
        zoom: 16
    });

    marker = new google.maps.Marker({
        position: izh,
        map: map,
        icon: 'images/landing-sprites/pin.png',
        title: 'Блок Аренда'
    });

    map.mapTypes.set('map_style', styledMap);
    map.setMapTypeId('map_style');

    google.maps.event.addDomListener(window, 'resize', function () {
        map.setCenter(izh);
    });
}

function toggleMobMenu() {

    var $btn = $('#js-mobMenuBtn'),
        $menu = $('#js-mobMenu');

    $btn.on('click', function () {
        $menu.parents('.top-header').toggleClass('opened');
    });
}

function scrollToFixedHeader() {

    var $fixedHeader = $('#js-fixedHeader'),
        $callbackFormBtn = $('#js-openCallbackFormBtn'),
        $callback = $('#js-topHeaderCallback');

    $fixedHeader.scrollToFixed();

    $callback.on('mouseenter', function () {
        $callbackFormBtn.fadeIn();
    });

    $(window).scroll(function () {
        if (!isMobile()) {
            $callbackFormBtn.fadeOut();
        }
    });
}

function scrollToSection() {

    var $navItems = $('#js-mobMenu > a'),
        $body = $('html, body'),
        scrollTop = 0,
        $sections = $('.wrapper > div');

    $navItems.on('click', function (e) {
        e.preventDefault();

        var id = $(this).attr('href');

        $navItems.removeClass('active');
        $(this).addClass('active');

        if (isMobile()) {
            $body.animate({
                scrollTop: $(id).position().top - 60
            }, 400, 'linear', function () {
                $('#js-mobMenu').parents('.top-header').removeClass('opened');
            });
        } else {
            $body.animate({
                scrollTop: $(id).position().top - 60
            }, 400, 'linear');
        }
    });

    $(window).on('scroll', function () {
        $navItems.removeClass('active');
        var windowScrollTop = $(this).scrollTop();
        if (windowScrollTop >= $('#characters').position().top - 60 && windowScrollTop < $('#transport').position().top - 60) {
            $('#js-mobMenu > a[href="#characters"]').addClass('active');
        } else if (windowScrollTop >= $('#transport').position().top - 60 && windowScrollTop < $('#rent').position().top - 60) {
            $('#js-mobMenu > a[href="#transport"]').addClass('active');
        } else if (windowScrollTop >= $('#rent').position().top - 60 && windowScrollTop < $('#contacts').position().top - 70) {
            $('#js-mobMenu > a[href="#rent"]').addClass('active');
        } else if (windowScrollTop >= $('#contacts').position().top - 60) {
            $('#js-mobMenu > a[href="#contacts"]').addClass('active');
        }
    });
}

function initTopShowcase() {

    $('#js-topShowcase').slick({
        dots: true,
        arrows: true,
        prevArrow: '<button type="button" class="slick-prev"><span class="icon"></span></button>',
        nextArrow: '<button type="button" class="slick-next"><span class="icon"></span></button>'
    });
}

function initCarousel() {

    $('#js-carousel').slick({
        dots: true,
        arrows: true,
        prevArrow: '<div class="slick-prev"><span class="icon"></span></div>',
        nextArrow: '<div class="slick-next"><span class="icon"></span></div>',
        centerMode: true,
        centerPadding: '0px',
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [{
            breakpoint: 800,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                centerMode: false
            }
        }]
    });
}

function initLightbox() {

    $('.js-imageLightbox').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeMarkup: '<button type="button" class="mfp-close"></button>',
        disableOn: function (e) {
            if ($(window).width() <= 800) {
                return false;
            }
            return true;
        }
    });
}

function validate(form) {

    $(form).validate({
        ignore: ".ignore",
        errorPlacement: function (error, element) {},
        rules: {
            name: {
                required: true
            },
            comment: {
                required: true
            },
            tel: {
                required: true
            }
        }
    });

    // Сообщения по-умолчанию
    $.extend($.validator.messages, {
        required: "Это поле необходимо заполнить."
    });
};

$(function () {
    toggleCallbackForm();
    initMap();
    toggleMobMenu();

    scrollToSection();

    initTopShowcase();

    initCarousel();

    initLightbox();

    scrollToFixedHeader();

    validate('form#callback');
    validate('form#contacts-form');

    $('input[type="tel"]').inputmask('+7 (999) 999-999');

    if ($(window).width() <= 800) {
        $('.js-imageLightbox').addClass('inactive');
        $('.js-imageLightbox').on('click', function (e) {
            e.preventDefault();
        });
    } else {
        $('.js-imageLightbox').removeClass('inactive');
    }

    $(window).resize(function () {
        if ($(window).width() <= 800) {
            $('.js-imageLightbox').addClass('inactive');
            $('.js-imageLightbox').on('click', function (e) {
                e.preventDefault();
            });
        } else {
            $('.js-imageLightbox').removeClass('inactive');
        }
    });
});
//# sourceMappingURL=main.js.map
