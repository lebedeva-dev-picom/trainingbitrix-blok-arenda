<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

	<div id="contacts" class="contacts">
		<div class="contacts__container">
		
			<div class="contacts__info">
				<h2>Контакты</h2>
				<div class="info-item">
					<div class="icon map"></div>
					<div class="info-item__text">г. Ижевск,<br/>ул.Ворошилова, 111А</div>
				</div>
				<div class="info-item">
					<div class="icon bubble"></div>
					<div class="info-item__text"> <a href="mailto:block.arenda@gmail.com" title="Написать">block.arenda@gmail.com</a><br><a href="tel:+79127601001" title="Позвонить">+7 (912) 760-1001</a></div>
				</div>
				<div class="info-item">
					<div class="icon user"></div>
					<div class="info-item__text">Гилязетдинов<br/>Илья Ильдусович</div>
				</div>
			</div>
			
			<!--div class="contacts__form">
			<form action="#" name="contacts_form" id="contacts-form">
				<fieldset>
					<h2>Оформить заявку на аренду</h2>
					<div class="field">
						<div class="field__two">
							<input type="text" placeholder="Имя*" name="name">
							<input type="text" placeholder="Организация" name="organization">
						</div>
					</div>
					<div class="field">
						<input type="tel" placeholder="Контактный телефон*" name="tel">
					</div>
					<div class="field field--comment">
						<textarea name="comment" placeholder="Объект и срок аренды*"></textarea>
					</div>
					<div class="field field--button">
						<input type="submit" value="Отправить" class="btn btn--big">
					</div>
				</fieldset>
				
				<h2>Оформить заявку на аренду</h2>
				<div class="triumph">
					<div class="triumph__name">Казимир Северинович,</div>
					<div class="triumph__message">Ваша заявка принята!</div>
					<div class="triumph__note">Наши менеджеры свяжутся с Вами в течении часа.</div>
				</div>
				<div class="btn btn--big">OK</div>
			</form>
			</div-->
			
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "include/contactsForm",
				"EDIT_TEMPLATE" => ""
			));?>
			
		</div>
	</div>
	
	<div id="map" style="width: 100%; height: 340px;" class="map"></div>
		<footer class="footer">
			<div class="footer__container"> 
				<p>&copy; Разработка сайта &mdash; <a href="http://picom.ru/" target="_blank" title="Пиком">&laquo;Пиком&raquo;</a></p>
			</div>
		</footer>
	</div>
	
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSi96kxgPpxGHn-lhfJhyIEtBTSqJMFhc"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery-scrolltofixed-min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/slick.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/magnific-popup.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.inputmask.bundle.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/jquery.validate.min.js"></script>
	<script src="<?=SITE_TEMPLATE_PATH?>/scripts/main.js"></script>
</body>
</html>