<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>

			<nav id="js-mobMenu">
				<?foreach($arResult as $arItem):?>
					<a href="<?=$arItem["LINK"]?>" title="<?=$arItem["TEXT"]?>"><span><?=$arItem["TEXT"]?></span></a>
				<?endforeach?>
			</nav>

<?endif?>