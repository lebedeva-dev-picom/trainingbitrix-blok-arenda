<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div id="js-carousel" class="carousel">

<?foreach($arResult["ITEMS"] as $arItem):?>

	<a href="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" style="background:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>') center no-repeat; background-size: cover;" class="js-imageLightbox carousel__item">
	<div class="carousel__zoom-icon"><span></span></div></a>

<?endforeach;?>

</div>
