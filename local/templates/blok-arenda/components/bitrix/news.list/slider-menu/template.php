<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<div id="js-topShowcase" class="top-showcase">
<?foreach($arResult["ITEMS"] as $arItem):?>

	<div style="background:url('<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>') center no-repeat; background-size: cover;" class="top-showcase__item">
		<div class="top-showcase-content">

			<div class="top-showcase-content__heading">
				<div class="icon stamp"></div>
				<div class="info">
					<h2><?echo $arItem["PREVIEW_TEXT"];?></h2>
					<p><?echo $arItem["NAME"];?></p>
				</div>
			</div>

			<div class="top-showcase-content__price">
				<div class="icon stamp"></div>
				<div class="rent-cost__item rent-cost__item--hour">
					<?echo $arItem["DETAIL_TEXT"];?>
				</div>
			</div>
		</div>
	</div>
	
<?endforeach;?>
</div>
