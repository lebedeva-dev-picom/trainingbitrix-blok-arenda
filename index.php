<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Блок Аренда");
?>
	<div id="characters" class="characters">
		<div class="characters__container">
		
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "include/characters_text1",
				"EDIT_TEMPLATE" => ""
			));?>
			
			<div class="characters__options">
				<div class="characters-options-item">
					<div class="characters-options-item__icon icon weight"></div>
					<div class="characters-options-item__info">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
							"AREA_FILE_SHOW" => "file", 
							"PATH" => "index_include/characters-options-item__weight.php", 
							"EDIT_TEMPLATE" => "",
						));?>
					</div>
				</div>
				
				<div class="characters-options-item">
					<div class="characters-options-item__icon icon length"></div>
					<div class="characters-options-item__info">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
							"AREA_FILE_SHOW" => "file", 
							"PATH" => "index_include/characters-options-item__length.php", 
							"EDIT_TEMPLATE" => "", 
						));?>
					</div>
				</div>
				
				<div class="characters-options-item">
					<div class="characters-options-item__icon icon store"></div>
					<div class="characters-options-item__info">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
							"AREA_FILE_SHOW" => "file", 
							"PATH" => "index_include/characters-options-item__store.php", 
							"EDIT_TEMPLATE" => "", 
						));?>
					</div>
				</div>
			</div>
		  
			<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
				"AREA_FILE_SHOW" => "page",
				"AREA_FILE_SUFFIX" => "include/characters_text2",
				"EDIT_TEMPLATE" => ""
			));?>
		  
        </div>
		
		
		<div class="characters__common-view">
			<div class="characters__container">
				
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "include/characters_images",
					"EDIT_TEMPLATE" => ""
				));?>
				
				<div class="tech-characters">
					<div class="tech-characters__heading">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
							"AREA_FILE_SHOW" => "page",
							"AREA_FILE_SUFFIX" => "include/characters_table_caption",
							"EDIT_TEMPLATE" => ""
						));?>
					</div>
					
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
						"AREA_FILE_SHOW" => "page",
						"AREA_FILE_SUFFIX" => "include/characters_table",
						"EDIT_TEMPLATE" => ""
					));?>
				</div>
			</div>
		</div>
	</div>
	
	
	
	<!--?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
	"AREA_FILE_SHOW" => "page",
	"AREA_FILE_SUFFIX" => "include/callbackForm",
	"EDIT_TEMPLATE" => ""
	));?-->
	
	<?$APPLICATION->IncludeComponent("bitrix:news.list", "slider-content", Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
			"ADD_SECTIONS_CHAIN" => "N",	// Включать раздел в цепочку навигации
			"AJAX_MODE" => "N",	// Включить режим AJAX
			"AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
			"AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
			"AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
			"AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
			"CACHE_FILTER" => "N",	// Кешировать при установленном фильтре
			"CACHE_GROUPS" => "Y",	// Учитывать права доступа
			"CACHE_TIME" => "36000000",	// Время кеширования (сек.)
			"CACHE_TYPE" => "N",	// Тип кеширования
			"CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
			"DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
			"DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
			"DISPLAY_DATE" => "N",	// Выводить дату элемента
			"DISPLAY_NAME" => "N",	// Выводить название элемента
			"DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
			"DISPLAY_PREVIEW_TEXT" => "N",	// Выводить текст анонса
			"DISPLAY_TOP_PAGER" => "N",	// Выводить над списком
			"FIELD_CODE" => array(	// Поля
				0 => "",
				1 => "",
			),
			"FILTER_NAME" => "",	// Фильтр
			"HIDE_LINK_WHEN_NO_DETAIL" => "N",	// Скрывать ссылку, если нет детального описания
			"IBLOCK_ID" => "6",	// Код информационного блока
			"IBLOCK_TYPE" => "media",	// Тип информационного блока (используется только для проверки)
			"INCLUDE_IBLOCK_INTO_CHAIN" => "N",	// Включать инфоблок в цепочку навигации
			"INCLUDE_SUBSECTIONS" => "N",	// Показывать элементы подразделов раздела
			"MESSAGE_404" => "",	// Сообщение для показа (по умолчанию из компонента)
			"NEWS_COUNT" => "20",	// Количество новостей на странице
			"PAGER_BASE_LINK_ENABLE" => "N",	// Включить обработку ссылок
			"PAGER_DESC_NUMBERING" => "N",	// Использовать обратную навигацию
			"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
			"PAGER_SHOW_ALL" => "N",	// Показывать ссылку "Все"
			"PAGER_SHOW_ALWAYS" => "N",	// Выводить всегда
			"PAGER_TEMPLATE" => ".default",	// Шаблон постраничной навигации
			"PAGER_TITLE" => "Новости",	// Название категорий
			"PARENT_SECTION" => "",	// ID раздела
			"PARENT_SECTION_CODE" => "",	// Код раздела
			"PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
			"PROPERTY_CODE" => array(	// Свойства
				0 => "",
				1 => "",
			),
			"SET_BROWSER_TITLE" => "N",	// Устанавливать заголовок окна браузера
			"SET_LAST_MODIFIED" => "N",	// Устанавливать в заголовках ответа время модификации страницы
			"SET_META_DESCRIPTION" => "N",	// Устанавливать описание страницы
			"SET_META_KEYWORDS" => "N",	// Устанавливать ключевые слова страницы
			"SET_STATUS_404" => "N",	// Устанавливать статус 404
			"SET_TITLE" => "N",	// Устанавливать заголовок страницы
			"SHOW_404" => "N",	// Показ специальной страницы
			"SORT_BY1" => "SORT",	// Поле для первой сортировки новостей
			"SORT_BY2" => "ID",	// Поле для второй сортировки новостей
			"SORT_ORDER1" => "ASC",	// Направление для первой сортировки новостей
			"SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
		),
		false
	);?>
		  
		  
	<div id="transport" class="transport">
	<div class="transport__container">
		<h2>Транспортировка</h2>
		<div class="weight-cards">
			<div class="weight-cards-item weight-cards-item--weight">
				<div class="weight-cards-item__content">
					<div class="count"> 
						<span class="icon bob"></span>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
							"AREA_FILE_SHOW" => "file", 
							"PATH" => "index_include/transport-weight.php", 
							"EDIT_TEMPLATE" => "",
						));?>
					</div>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
						"AREA_FILE_SHOW" => "file", 
						"PATH" => "index_include/transport-weight-text.php", 
						"EDIT_TEMPLATE" => "",
					));?>
				</div>
			</div>
			<div class="weight-cards-item weight-cards-item--dims">
				<div class="weight-cards-item__content">
					<div class="count"> 
						<span class="icon measure"></span>
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
							"AREA_FILE_SHOW" => "file", 
							"PATH" => "index_include/transport-dims.php", 
							"EDIT_TEMPLATE" => "",
						));?>
					</div>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
						"AREA_FILE_SHOW" => "file", 
						"PATH" => "index_include/transport-dims-text.php", 
						"EDIT_TEMPLATE" => "",
					));?>
				</div>
			</div>
		</div>
		<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
			"AREA_FILE_SHOW" => "page",
			"AREA_FILE_SUFFIX" => "include/transport-description",
			"EDIT_TEMPLATE" => ""
		));?>
	</div>
	</div>
	  
	  
	<div id="rent" class="rent">
		<div class="rent__container">
			<h2>Стоимость аренды</h2>
			<p><span class="icon worker"></span><span>Стоимость аренды трубоукладчика с экипажем</span></p>
		</div>
		<div class="rent-cost">
			<div class="rent-cost__item rent-cost__item--hour">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
					"AREA_FILE_SHOW" => "file", 
					"PATH" => "index_include/rent-cost-hour.php", 
					"EDIT_TEMPLATE" => "",
				));?>
			</div>
			<div class="rent-cost__item rent-cost__item--tour">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
					"AREA_FILE_SHOW" => "file", 
					"PATH" => "index_include/rent-cost-tour.php", 
					"EDIT_TEMPLATE" => "",
				));?>
			</div>
			<div class="rent-cost__item rent-cost__item--month">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "",Array(
					"AREA_FILE_SHOW" => "file", 
					"PATH" => "index_include/rent-cost-month.php", 
					"EDIT_TEMPLATE" => "",
				));?>
			</div>
			<div class="rent-cost__note">
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "include/rent-cost-note",
					"EDIT_TEMPLATE" => ""
				));?>
			</div>
		</div>
		<div class="rent__container">
			<div class="rent__bottom">
				<p><span class="icon oil"></span>
				<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "include/rent-oil",
					"EDIT_TEMPLATE" => ""
				));?></p>
				<p><span class="icon contract"></span>
					<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
						"AREA_FILE_SHOW" => "page",
						"AREA_FILE_SUFFIX" => "include/rent-contract",
						"EDIT_TEMPLATE" => ""
					));?>
					<span class="note">
						<?$APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
							"AREA_FILE_SHOW" => "page",
							"AREA_FILE_SUFFIX" => "include/rent-contract-note",
							"EDIT_TEMPLATE" => ""
						));?>
					</span>
				</p>
			</div>
		</div>
	</div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>