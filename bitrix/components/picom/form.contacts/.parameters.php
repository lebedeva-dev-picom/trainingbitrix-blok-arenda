<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = array(
	"GROUPS"    => array(),
	"PARAMETERS" => array(
		"AJAX_MODE" => array(),
		"MAIL_TO"   => array(
			"PARENT"   => "BASE",
			"NAME"     => "Email отправителя",
			"TYPE"     => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT"  => "",
		),
		"MAIL_FROM" => array(
			"PARENT"   => "BASE",
			"NAME"     => "Email получателя",
			"TYPE"     => "STRING",
			"MULTIPLE" => "N",
			"DEFAULT"  => "",
		),
	),
);

?>