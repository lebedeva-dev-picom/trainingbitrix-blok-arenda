$(function(){

$('#contacts-form').submit(submit_contacts_form);

});


function showContactsForm(event)
{
	//var fildset = $('#contacts-form fieldset');
	//$('#contacts-form').html($('#contacts-form fieldset').html());
	//$('#contacts-form fieldset').show();
}


function submit_contacts_form(event)
{
	var path = 'bitrix/components/picom/form.contacts/templates/.default/validForm.php';
	var data = {
		'name'         : this.name.value,
		'organization' : this.organization.value,
		'tel'          : this.tel.value,
		'comment'      : this.comment.value,
		'mail_to'      : mail_to,
		'mail_from'    : mail_from
	};
	BX.ajax.post(path, data, success);
	event.preventDefault();
}


function success(e)
{
	var r = JSON.parse(e);
		
	if (r.name_err) { $('#contacts-form [name="name"]').addClass('error'); }
	if (r.tel_err) { $('#contacts-form [name="tel"]').addClass('error'); }
	if (r.comment_err) { $('#contacts-form [name="comment"]').addClass('error'); }
	
	//console.log(r.log);
	
	if (r.send)
	{
		var name = r.name;
		
		//$('#contacts-form fieldset').hide();
		$('#contacts-form').html(
		'<h2>Оформить заявку на аренду</h2>' +
		'<div class="triumph">' +
			'<div class="triumph__name">' + name + ',</div>' +
			'<div class="triumph__message">Ваша заявка принята!</div>' +
			'<div class="triumph__note">Наши менеджеры свяжутся с Вами в течении часа.</div>' +
		'</div>' +
		'<div class="btn btn--big">OK</div>');
		//$('#contacts-form div').find('.btn btn--big').get(0).submit(showContactsForm);
	}
	else
	{
		//alert('не удалось отправить сообщение');
	}
}



