<?if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();?>


<div class="contacts__form">
<form action="#" name="contacts_form" id="contacts-form">
	
	<fieldset>
		<h2>Оформить заявку на аренду</h2>
		<div class="field">
			<div class="field__two">
				<input type="text" placeholder="Имя*" name="name">
				<input type="text" placeholder="Организация" name="organization">
			</div>
		</div>
		<div class="field">
			<input type="tel" placeholder="Контактный телефон*" name="tel">
		</div>
		<div class="field field--comment">
			<textarea name="comment" placeholder="Объект и срок аренды*"></textarea>
		</div>
		<div id="tst"></div>
		<div class="field field--button">
			<input type="submit" value="Отправить" class="btn btn--big">
		</div>
	</fieldset>

</form>
</div>



<script>
	var mail_to = '<? echo $arParams["MAIL_TO"]; ?>';
	var mail_from = '<? echo $arParams["MAIL_FROM"]; ?>';
</script>