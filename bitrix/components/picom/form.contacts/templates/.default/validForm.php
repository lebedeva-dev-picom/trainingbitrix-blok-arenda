<?
// if(!defined("B_PROLOG_INCLUDED")||B_PROLOG_INCLUDED!==true)die();
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
?>

<?
$name = htmlspecialchars(strip_tags($_POST['name']));
$organization = htmlspecialchars(strip_tags($_POST['organization']));
$tel = htmlspecialchars(strip_tags($_POST['tel']));
$comment = htmlspecialchars(strip_tags($_POST['comment']));

$mail_to = $_POST['mail_to'];
$mail_from = $_POST['mail_from'];

?>

<?

$name_err = chkName($name);
$tel_err = chkTel($tel);
$comment_err = chkComm($comment);

if (!($name_err || $tel_err || $comment_err))
{
	if (add_iblock())
		$sendMailResult = send_mail();
	else $sendMailResult = false;
}
else $sendMailResult = false;

$result = array(
	'name'        => $name,
	'name_err'    => $name_err,
	'tel_err'     => $tel_err,
	'comment_err' => $comment_err,
	'send'        => $sendMailResult,
	'log'         => $err,
);

echo json_encode($result);
?>


<?
function  add_iblock()
{
	global $log, $name, $organization, $tel, $comment, $mail_to, $mail_from, $err;
	
	$iblockElementId = -1;
	$iblockName = 'Новая заявка на аренду';
	$iblockId = 7;  //FORM_RENT
	$userId = 1;  //admin
	$nowDateTime = date('DD.MM.YYYY HH:MI:SS');
	
	CModule::IncludeModule("iblock");
	$iblockElement = new CIBlockElement();
	
	$arFields = array(
	'ID'                => $iblockElementId,
	'NAME'              => $iblockName,
	'IBLOCK_ID'         => $iblockId,
	'ACTIVE'            => 'Y',
	'SORT'              => 500,
	'PREVIEW_TEXT_TYPE' => 'text',
	'DETAIL_TEXT_TYPE'  => 'text',
	'DATE_CREATE'       => $nowDateTime,
	'CREATED_BY'        => $userId,
	'TIMESTAMP_X'       => $nowDateTime,
	'MODIFIED_BY'       => $userId,
	'WF_STATUS_ID'      => 1,
	'PROPERTY_VALUES' => array(
		'NAME'         => $name,
		'ORGANIZATION' => $organization,
		'TEL'          => $tel,
		'COMMENT'      => $comment,
		),
	);
	
	$iblockElementId = $iblockElement->add($arFields);
	$err = $iblockElement->LAST_ERROR;
	
	if($iblockElementId) return true;
	else return false;
}


function send_mail()
{
	global $log, $name, $organization, $tel, $comment, $mail_to, $mail_from;
	
	$send_id = false;
	
	$arEventFields = array(
		'NAME'         => $name,
		'ORGANIZATION' => $organization,
		'TEL'          => $tel,
		'COMMENT'      => $comment,
		'MAIL_TO'      => $mail_to,
		'MAIL_FROM'    => $mail_from,
	);
	CModule::IncludeModule("main");
	$send_id = CEvent::Send('SEND_CONTACTS_FORM', 's1', $arEventFields);
	
	if ($send_id) return true;
	else return false;
}
?>


<?

function chkName($p)
{
	if($p=='') return true;
	else return false;
}

function chkTel($p)
{
	//+7 (555) 555-555
	$reg = '/\+7\s\(\d{3}\)\s\d{3}\-\d{3}/';
	preg_match($reg, $p, $arr);

	if($p=='' || $arr[0] != $p) return true;
	else return false;
}

function chkComm($p)
{
	if($p=='') return true;
	else return false;
}
?>
