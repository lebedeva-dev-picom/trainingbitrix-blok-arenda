<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
CUtil::InitJSCore(array('window', 'ajax'));
?>

<div id="js-callbackForm" class="callback">
<div class="callback__form">
	<form action="#" name="callback" id="callback">
	<fieldset>
		<h2>Обратный звонок</h2>
		<div class="field">
		<input type="text" placeholder="Как Вас зовут?*" name="name">
		</div>
		
		<div class="field">
		<input type="tel" placeholder="Какой у Вас номер телефона?*" name="tel">
		</div>
		
		<div class="field field--button">
		<input type="submit" value="Отправить" class="btn btn--big">
		</div>
		
		<div class="note">Наши менеджеры свяжутся с Вами в течении часа.</div>
	</fieldset>
	</form>
	
	<!--div class="triumph">
		<div class="triumph__name">Казимир Северинович,</div>
		<div class="triumph__message">спасибо за Ваше обращение!</div>
		<div class="triumph__note">Наши менеджеры свяжутся с Вами в течении часа.</div>
	</div-->
	<!--div class="btn btn--big">Закрыть</div-->
	<!--a href="#close-callback" title="Закрыть" id="js-closeCallbackFormBtn" class="callback__close-btn"><span class="icon close"></span></a-->
	
</div>
</div>


<script>
	var mail_to = '<? echo $arParams["MAIL_TO"]; ?>';
	var mail_from = '<? echo $arParams["MAIL_FROM"]; ?>';
	//var path = '<? $this->GetFolder(); ?>';
	var path = '/bitrix/components/picom/form.callback/templates/.default';
</script>




