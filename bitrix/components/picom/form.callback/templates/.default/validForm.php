<?require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");?>

<?
$name = htmlspecialchars(strip_tags($_POST['name']));
$tel = htmlspecialchars(strip_tags($_POST['tel']));
$mail_to = $_POST['mail_to'];
$mail_from = $_POST['mail_from'];

echo json_encode(result());
?>

<?
function result()
{
	global $name, $tel, $log;
	
	$name_err = chkName($name);
	$tel_err = chkTel($tel);
	
	if(!($name_err || $tel_err))
	{
		if (add_iblock())
			$sendMailResult = send_mail();
		else $sendMailResult = false;
	}
	else $sendMailResult = false;
	
	return array(
		'name'     => $name,
		'name_err' => $name_err,
		'tel_err'  => $tel_err,
		'send'     => $sendMailResult,
		'log'      => $log,
	);
}
?>


<?
function  add_iblock()
{
	global $name, $tel, $log;
	
	$iblockId = 8;  //FORM_CALLBACK
	$userId = 1;  //admin
	$nowDateTime = date('DD.MM.YYYY HH:MI:SS');
	
	CModule::IncludeModule("iblock");
	$iblockElement = new CIBlockElement();
	
	$arFields = array(
	'ID'                => -1,
	'NAME'              => 'Новый заказ звонка',
	'IBLOCK_ID'         => $iblockId,
	'ACTIVE'            => 'Y',
	'SORT'              => 500,
	'PREVIEW_TEXT_TYPE' => 'text',
	'DETAIL_TEXT_TYPE'  => 'text',
	'DATE_CREATE'       => $nowDateTime,
	'CREATED_BY'        => $userId,
	'TIMESTAMP_X'       => $nowDateTime,
	'MODIFIED_BY'       => $userId,
	'WF_STATUS_ID'      => 1,
	'PROPERTY_VALUES' => array(
		'NAME'         => $name,
		'TEL'          => $tel,
		),
	);
	
	$iblockElementId = $iblockElement->add($arFields);
	$log = $iblockElement->LAST_ERROR;
	
	if($iblockElementId) return true;
	else return false;
}

function send_mail()
{
	global $name, $tel, $mail_to, $mail_from, $log;
	
	$send_id = false;
	
	$arEventFields = array(
		'NAME'         => $name,
		'TEL'          => $tel,
		'MAIL_TO'      => $mail_to,
		'MAIL_FROM'    => $mail_from,
	);
	CModule::IncludeModule("main");
	$send_id = CEvent::Send('CALLBACK', 's1', $arEventFields);
	
	if ($send_id) return true;
	else return false;
}

?>


<?
function chkName($p)
{
	if($p=='') return true;
	else return false;
}

function chkTel($p)
{
	//+7 (555) 555-555
	$reg = '/\+7\s\(\d{3}\)\s\d{3}\-\d{3}/';
	preg_match($reg, $p, $arr);

	if($p=='' || $arr[0] != $p) return true;
	else return false;
}
?>