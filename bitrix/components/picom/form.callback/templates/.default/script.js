$(function(){
	$('#callback').submit(submit_callback_form);
});

function submit_callback_form(event)
{
	var data = {
		'name'         : this.name.value,
		'tel'          : this.tel.value,
		'mail_to'      : mail_to,
		'mail_from'    : mail_from
	};
	
	BX.ajax.post(
		path + '/validForm.php', 
		data,
		function(e)
		{
			var result = JSON.parse(e);
			
			// console.log('name: ' + result.name);
			// console.log('name_err: ' + result.name_err);
			// console.log('tel_err: ' + result.tel_err);
			// console.log('send: ' + result.send);
			// console.log('log: ' + result.log);
			
			if(result.name_err) $('#callback [name="name"]').addClass('error');
			if(result.tel_err)  $('#callback [name="tel"]').addClass('error');
			
			if(result.send)
			{
				var name = result.name;
				
				$('.callback__form #callback').hide();
				
				$('.callback__form').append(
				'<div class="triumph">' +
					'<div class="triumph__name">' + name + ',</div>' +
					'<div class="triumph__message">спасибо за Ваше обращение!</div>' +
					'<div class="triumph__note">Наши менеджеры свяжутся с Вами в течении часа.</div>' +
				'</div>' +
				'<div class="btn btn--big">Закрыть</div>' +
				'<a href="#close-callback" title="Закрыть" id="js-closeCallbackFormBtn" class="callback__close-btn"><span class="icon close"></span></a>');
			}
		}
	);
	event.preventDefault();
}

function success(e)
{
}