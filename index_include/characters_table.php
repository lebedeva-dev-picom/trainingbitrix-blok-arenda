<table>
<tbody>
<tr>
	<td>
		Грузоподъемность
	</td>
	<td>
 <strong>72</strong>&nbsp;тонны
	</td>
</tr>
<tr>
	<td>
		Масса полностью укомплектованной машины
	</td>
	<td>
 <strong>51</strong>&nbsp;тонна
	</td>
</tr>
<tr>
	<td>
		Длина стрелы
	</td>
	<td>
 <strong>7,31 </strong>&nbsp;метра
	</td>
</tr>
<tr>
	<td>
		Максимальная высота подъема крюка
	</td>
	<td>
 <strong>6,5 </strong>&nbsp;метра
	</td>
</tr>
<tr>
	<td>
		Вылет минимальный
	</td>
	<td>
 <strong>1,22 </strong>&nbsp;метра
	</td>
</tr>
<tr>
	<td>
		Вылет максимальный
	</td>
	<td>
 <strong>7,31 </strong>&nbsp;метра
	</td>
</tr>
<tr>
	<td>
		Грузоподъёмность на максимальном вылете
	</td>
	<td>
 <strong>11,32 </strong>&nbsp;тонны
	</td>
</tr>
<tr>
	<td>
		Окружающая среда, в которой может работать кран
	</td>
	<td>
		от <strong>-40</strong>&nbsp; до <strong>+40 </strong>°С
	</td>
</tr>
<tr>
	<td>
		Объем топливного бака
	</td>
	<td>
 <strong>700 </strong>&nbsp;литров
	</td>
</tr>
</tbody>
</table>